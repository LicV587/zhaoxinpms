package com.zhaoxinms.workflow.engine.model.designer;

public class ProcessData extends Node{

    private StartProperties properties;

    public StartProperties getProperties() {
        return properties;
    }

    public void setProperties(StartProperties properties) {
        this.properties = properties;
    }
}