package com.zhaoxinms.workflow.engine.model.designer;

import java.util.List;

public class ConditionNode extends Node {

    private ConditionProperties properties;

    public ConditionProperties getProperties() {
        return properties;
    }

    public void setProperties(ConditionProperties properties) {
        this.properties = properties;
    }
}