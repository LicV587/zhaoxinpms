package com.zhaoxinms.workflow.engine.model.designer;

public class Conditions {

    private String condition;
    private String conditonLabel;

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCondition() {
        return condition;
    }

    public void setConditonLabel(String conditonLabel) {
        this.conditonLabel = conditonLabel;
    }

    public String getConditonLabel() {
        return conditonLabel;
    }

}