package com.zhaoxinms.workflow.engine.model.designer;

public class StartProperties {

    private String title;
    private String initiator;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getInitiator() {
        return initiator;
    }
    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
}